\documentclass[12pt,a4paper]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[italian]{babel}
\usepackage{xcolor}
\usepackage{listings}
\usepackage{paralist}
\usepackage[colorlinks=true,linkcolor=black,urlcolor=blue]{hyperref}

\lstdefinestyle{bash}{
	literate={~} {$\sim$}{1}, 	% fix per le tilde
	showstringspaces=false,		% fix per gli spazi vuoti
	breaklines=true,			% fix per le linee di codice che passano il documento
	language=bash,
	basicstyle=\small\sffamily,
	numbers=left,
	numberstyle=\tiny,
	numbersep=3pt,
	frame=tb,
	columns=fullflexible,
	backgroundcolor=\color{yellow!20},
	linewidth=1\linewidth,
	xleftmargin=0\linewidth
}

\begin{document}
	\title{Creazione nuovo progetto}
	\author{Mauro Dattilo}
	\maketitle
	\clearpage

	\tableofcontents
	\clearpage

	\section{Introduzione}
		Questi appunti sono una guida per la creazione di un nuovo progetto, partendo dalla configurazione del database fino al codice vero e proprio.\newline
		Ho saltato volutamente alcune nozioni di teoria per non renderli troppo prolissi: lo scopo di questi appunti, infatti, è creare e configurare nel minore tempo possibile un nuovo progetto per cominciare a lavorare.\newline
		Per qualsiasi informazione aggiuntiva, vi invito a leggere le documentazioni ufficiali.
	\clearpage

	\section{Configurazioni generali}
		\subsection{Creazione ambiente virtuale}
			La prima cosa da fare è creare i due ambienti virtuali che conterranno le librerie e gli eseguibili Python per il progetto. Lanciamo il comando:
			\begin{lstlisting}[style=bash,gobble=24]
			 $ mkvirtualenv nomeprogetto_dev
			 $ deactivate
			 $ mkvirtualenv nomeprogetto_test
			 $ deactivate
			\end{lstlisting}
			che creerà, oltre ai due ambienti virtuali per lo sviluppo e il testing dell'applicazione, anche la directory "\emph{progetto}" (sia "\emph{\_dev}" che "\emph{\_test}" verranno rimossi dal nome della cartella) all'interno di \$PROJECT\_HOME (la cartella contenente i nostri progetti).

		\subsection{Creazione e configurazione database}
			Useremo il DBMS PostgreSQL: dobbiamo creare un database, creare un utente ed assegnargli i permessi di accesso sul database. Effettuiamo il login come l'utente \emph{postgres} tramite:
			\begin{lstlisting}[style=bash,gobble=24]
			 $ sudo su - postgres
			\end{lstlisting}
			Il terminale cambierà in "\emph{postgres@hostname}" (dove hostname è il nome che avete dato alla vostra macchina). In questo caso, procediamo con la creazione del nuovo utente:
			\begin{lstlisting}[style=bash,gobble=24]
			 $ createuser --interactive -P
			\end{lstlisting}
			Partirà una procedura che vi chiederà le seguenti cose:
			\begin{compactitem}
				\item \textbf{Enter name of role to add} dove inseriamo il nome del nuovo utente;
				\item \textbf{Enter password for new role} dove inseriamo la password del nuovo utente (prendiamo una buona password da \href{https://www.grc.com/passwords.htm}{quì});
				\item \textbf{Enter it again} dove inseriamo di nuovo la password (per la verifica);
				\item \textbf{Shall the new role be a superuser? (y/n)} dove decidiamo se il nuovo utente è anche amministratore (no);
				\item \textbf{Shall the new role be allowed to create databases? (y/n)} dove decidiamo se il nuovo utente può creare altri database (sì, perchè per creare il database dei test di Django, l'utente creato ha bisogno di questi permessi);
				\item \textbf{Shall the new role be allowed to create more new roles? (y/n)} dove decidiamo se il nuovo utente può creare altri ruoli (no);
			\end{compactitem}
			Adesso possiamo creare il nuovo database ed assegnare il proprietario con:
			\begin{lstlisting}[style=bash,gobble=24]
			 $ createdb --owner nuovo_utente nome_progetto
			\end{lstlisting}
			Usciamo con:
			\begin{lstlisting}[style=bash,gobble=24]
			 $ logout
			\end{lstlisting}	
	\clearpage

	\section{Creazione progetto}
		Adesso possiamo cominciare a creare e configurare il nostro progetto Django. Entriamo nell'ambiente virtuale di sviluppo con il comando:
		\begin{lstlisting}[style=bash,gobble=16]
		 $ workon nomeprogetto_dev
		\end{lstlisting}
		che ci porterà nella cartella "\emph{nomeprogetto}": in questa cartella verranno salvati tutti i file del progetto.
		\subsection{Creazione progetto con makeproject}
			Una volta entrati nell'ambiente virtuale, lanciamo il comando
			\begin{lstlisting}[style=bash,gobble=24]
			 $ pip install django
			\end{lstlisting}
			per installare l'ultima versione di Django. Potete controllare che l'installazione sia andata a buon fine attraverso i comandi:
			\begin{lstlisting}[style=bash,gobble=24]
			 $ python
			 >>> import django
			 >>> django.get_version()
			 '1.8.5'
			 >>> exit()
			\end{lstlisting}
			Adesso creiamo lo scheletro del progetto lanciando
			\begin{lstlisting}[style=bash,gobble=24]
			 $ django-admin.py startproject nomeprogetto
			\end{lstlisting}
			all'interno della cartella root del nostro progetto.\newline
			Infine creiamo il file .gitignore nella root del progetto tramite PyCharm.
		\subsection{Creazione progetto con Cookiecutter}
			Installiamo "\emph{cookiecutter}" per generare il progetto da template già esistenti:
			\begin{lstlisting}[style=bash,gobble=24]
			 $ pip install cookiecutter
			\end{lstlisting}
			E quindi creiamo la struttura con:
			\begin{lstlisting}[style=bash,gobble=24]
			 $ cd $PROJECT_HOME
			 $ cookiecutter https://github.com/pydanny/cookiecutter-django.git
			\end{lstlisting}
			Adesso \emph{cookiecutter} provvederà a scaricare il template del progetto e a configurarlo con una procedura guidata, dove verranno chieste alcune informazioni: dobbiamo prestare attenzione alla voce \textbf{repo\_name} perchè dovremo inserire lo stesso nome dato all'ambiente virtuale in modo che la cartella creata da \emph{cookiecutter} abbia lo stesso nome di quella creata da \emph{virtualenvwrapper}.\newline
			Adesso per installare nell'ambiente virtuale tutte le app che servono al progetto stesso (inclusa \emph{Django}) lanciamo il comando:
			\begin{lstlisting}[style=bash,gobble=24]
			 $ pip install -r requirements/local.txt
			\end{lstlisting}
		\subsection{Progetto esistente}
	\clearpage
	
	\section{Configurazione virtualenvwrapper}
		Creato il progetto, non ci resta che configurare le variabili d'ambiente per tenerle fuori dal software di controllo di versione che andremo ad utilizzare. 
		\subsection{Configurazione ambiente di sviluppo}
			Entriamo nell'ambiente di sviluppo del nostro progetto con:
			\begin{lstlisting}[style=bash,gobble=24]
			 $ workon nomeprogetto_dev
			\end{lstlisting}
			e andiamo a modificare il file postactivate di questo ambiente con i comandi:
			\begin{lstlisting}[style=bash,gobble=24]
			 $ cd $VIRTUAL_ENV/bin
			 $ mousepade postactivate
			\end{lstlisting}
			e aggiungiamo le seguenti linee alla fine del file:
			\begin{lstlisting}[style=bash,gobble=24]
			 export DJANGO_SETTING_MODULE='indirizzo.al.file.di.impostazioni.dev'
			 export SECRET_KEY='la chiave segreta che django genera automaticamente'
			\end{lstlisting}
			Sostituire i valori tra virgolette con quelli del progetto in sviluppo.
			\begin{lstlisting}[style=bash,gobble=24]
			 $ mousepade predeactivate
			\end{lstlisting}
			e aggiungiamo le seguenti linee alla fine del file:
			\begin{lstlisting}[style=bash,gobble=24]
			 unset DJANGO_SETTING_MODULE
			 unset SECRET_KEY
			\end{lstlisting}
		\subsection{Configurazione ambiente di testing}
			Entriamo nell'ambiente testing del nostro progetto con:
			\begin{lstlisting}[style=bash,gobble=24]
			 $ workon nomeprogetto_test
			\end{lstlisting}
			e andiamo a modificare il file postactivate di questo ambiente con i comandi:
			\begin{lstlisting}[style=bash,gobble=24]
			 $ cd $VIRTUAL_ENV/bin
			 $ mousepade postactivate
			\end{lstlisting}
			e aggiungiamo le seguenti linee alla fine del file:
			\begin{lstlisting}[style=bash,gobble=24]
			 export DJANGO_SETTING_MODULE='indirizzo.al.file.di.impostazioni.testing'
			 export SECRET_KEY='la chiave segreta che django genera automaticamente'
			\end{lstlisting}
			Sostituire i valori tra virgolette con quelli del progetto in sviluppo.
			\begin{lstlisting}[style=bash,gobble=24]
			 $ mousepade predeactivate
			\end{lstlisting}
			e aggiungiamo le seguenti linee alla fine del file:
			\begin{lstlisting}[style=bash,gobble=24]
			 unset DJANGO_SETTING_MODULE
			 unset SECRET_KEY
			\end{lstlisting}
	\clearpage
	
	\section{Configurazione PostgreSQL}
		Per configurare PostgreSQL, dobbiamo installare prima il driver per la comunicazione con Django (se è stata fatta la creazione attraverso cookiecutter, saltare questo passaggio).
		Lanciamo il comando:
		\begin{lstlisting}[style=bash,gobble=16]
		 $ pip install psycopg2
		\end{lstlisting}
		Adesso possiamo configurare Django per comunicare con PostgreSQL.
		Apriamo PyCharm, clicchiamo su \textbf{Open\dots} e selezionamo la cartella del progetto.
		\subsection{File impostazioni makeproject}
			Apriamo il file \textbf{settings.py} e cerchiamo le seguenti righe di codice:
			\begin{lstlisting}[style=bash,gobble=24]
			 DATABASES = {
				 'default': {
					 'ENGINE': 'django.db.backends.sqlite3',
					 'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
				 }
			 }
			\end{lstlisting}
			per cambiarle in:
			\begin{lstlisting}[style=bash,gobble=24]
			 DATABASES = {
				 'default': {
				 	 'ENGINE': 'django.db.backends.postgresql_psycopg2',
					 'NAME': 'nome_database',
					 'USER': 'nome_utente',
					 'PASSWORD': 'password_utente',
					 'HOST': 'localhost',
					 'PORT': '',
				 }
			 }
			\end{lstlisting}

		\subsection{File impostazioni cookiecutter}
			Apriamo il file \textbf{config} $\rightarrow$ \textbf{common.py} e cerchiamo le seguenti righe di codice:
			\begin{lstlisting}[style=bash,gobble=24]
			 # DATABASE CONFIGURATION
			 # See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
			 DATABASES = values.DatabaseURLValue('postgres://localhost/repo_name')
			 # END DATABASE CONFIGURATION
			\end{lstlisting}
			per cambiarle in:
			\begin{lstlisting}[style=bash,gobble=24]
			 # DATABASE CONFIGURATION
			 # See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
			 DATABASES = values.DatabaseURLValue('postgres://nuovo_utente:password_utente@localhost/nome_database')
			 # END DATABASE CONFIGURATION
			\end{lstlisting}
		\subsection{Configurazioni finali}
			Adesso possiamo lanciare il comando per creare la migrazione, le tabelle e popolare il database:
			\begin{lstlisting}[style=bash,gobble=24]
			 $ python nome_progetto/manage.py migrate
			\end{lstlisting}
			Lanciamo il comando per creare il superuser:
			\begin{lstlisting}[style=bash,gobble=24]
			 $ python nome_progetto/manage.py createsuperuser
			\end{lstlisting}
			Lanciamo il comando per far partire il webserver integrato di Django:
			\begin{lstlisting}[style=bash,gobble=24]
			 $ python nome_progetto/manage.py runserver
			\end{lstlisting}
	\clearpage

	\section{Configurazione GIT}
		Adesso non ci resta altro che usare un CVS per il nostro codice: GIT.
		Per farlo, andiamo nella root del nostro progetto ed inizializziamo il repository con:
		\begin{lstlisting}[style=bash,gobble=16]
		 $ git init
		\end{lstlisting}
		aggiungiamo i file allo stage e facciamo il commit con:
		\begin{lstlisting}[style=bash,gobble=16]
		 $ git add .
		 $ git commit -m "Generato lo scheletro dell'applicazione"
		\end{lstlisting}
		Adesso possiamo collegare a BitBucket attraverso il comando "\emph{remote add}" e pushare i file su questo sito:
		\begin{lstlisting}[style=bash,gobble=16]
		 $ git remote add origin LINK_AL_REPOSITORY
		 $ git push -u origin master
		\end{lstlisting}
	\clearpage
\end{document}